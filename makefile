WORDSRC != echo *.md
WORDS = ${WORDSRC:.md=.html}
GEN = ${WORDS} index.html feed.atom

all: ${GEN}

.SUFFIXES: .md .html

.md.html:
	lowdown -sthtml -M css=style.css -M title=$< $< > $@

index.html: ${WORDS} dirlist.sh
	./dirlist.sh > $@

feed.atom: ${WORDS} feed.sh
	./feed.sh > $@

clean:
	rm -f ${GEN}

.PHONY: all clean
